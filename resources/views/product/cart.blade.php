@extends('layouts.master')
@section('title') Cart @endsection

@section('content')
<style type="text/css">
  .panel-title {
  display: inline;
  font-weight: bold;
  }
  .display-table {
      display: table;
  }
  .display-tr {
      display: table-row;
  }
  .display-td {
      display: table-cell;
      vertical-align: middle;
      width: 61%;
  }
</style>
<div class="page-section">
    <div class="container">
      @if(Session::has('message'))
       <div class="alert alert-danger">
         {{ Session::get('message')}}
       </div>
      @endif
      @if(Session::has('error'))
       <div class="alert alert-danger">
         {{ Session::get('error')}}
       </div>
      @endif
      @if(Session::has('success'))
       <div class="alert alert-success">
         {{ Session::get('success')}}
       </div>
      @endif
      <div class="row">
          <table class="table table-hover">
              <thead>
                  <tr>
                      <th width="50%">Product</th>
                      <th width="10%">Price</th>
                      <th width="8%">Quantity</th>
                      <th width="22%">Sub Total</th>
                      <th width="10%"></th>
                  </tr>
              </thead>
              <tbody>
                 @php $total = 0; @endphp
                 @if(session('cart'))
                 @foreach(session('cart') as $id => $product)
                 @php
                     $subtotal = $product['price'] * $product['quantity'];
                     $total += $subtotal;
                 @endphp
                 <tr>
                     <td><img src="{{ $product['image'] }}" alt="{{ $product['name'] }}" width="150">
                        <span>{{ $product['name'] }}</span>
                     </td>
                     <td>${{ $product['price'] }}</td>
                     <td>
                       <form action="{{ route('change_qty',$id) }}" class="d-flex">
                      <button type="submit" name="change_to" value="down" class="btn btn-danger">
                        @if($product['quantity'] === 1) x @else - @endif
                      </button>
                      <input type="number" value="{{ $product['quantity'] }}" disabled>
                      <button type="submit" value="up" name="chamge_to" class="btn btn-success">+</button>
                    </form>
                     </td>
                     <td>${{ $subtotal }}</td>
                     <td>
                         <a href="{{ route('remove',$id) }}" class="btn btn-danger btn-sm">X</a>
                     </td>
                 </tr>
                 @endforeach
                 @endif
              </tbody>
              <tfoot>
                  <tr>
                      <td style="display: block ruby;">
                          <a href="{{ route('Product') }}" class="btn btn-warning">Continue Shopping</a>
                            <form method="post" action="{{ route('Pay') }}">
                                @csrf
                                <input type="hidden" name="amount" value={{ $total }}>
                              <button type="submit" class="btn btn-success">Pay With Paypal</button>
                            </form>
                            <button class="btn btn-info" data-toggle="modal" data-target="#paymentStripe" >Pay With Stripe</a>
                        </td>
                        
                      <td colspan="2"></td>
                      <td><strong>Total ${{ $total }}</strong></td>
                  </tr>
              </tfoot>
          </table>
      </div> 
    
    </div>
  </div>

  <!-- Modal -->
<div class="modal fade" id="paymentStripe" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Payment Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form 
                            role="form" 
                            action="{{ route('Pay_Stripe') }}" 
                            method="post" 
                            class="require-validation"
                            data-cc-on-file="false"
                            data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                            id="payment-form">
                        @csrf
  
                        
                            <div class='form-group'>
                                <label class='control-label'>Name on Card</label> <input
                                    class='form-control' size='4' type='text' required>
                            </div>
                        
  
                     
                            <div class='col-xs-12 form-group'>
                                <label class='control-label'>Card Number</label> <input
                                    autocomplete='off' class='form-control card-number' size='20'
                                    type='text' required>
                            </div>
                       
  
                        <div class='row'>
                            <div class='col-xs-12 col-md-4 form-group cvc'>
                                <label class='control-label'>CVC</label> <input autocomplete='off'
                                    class='form-control card-cvc' placeholder='ex. 311' size='4'
                                    type='text' required>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration'>
                                <label class='control-label'>Expiration Month</label> <input
                                    class='form-control card-expiry-month' placeholder='MM' size='2'
                                    type='text' required>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration'>
                                <label class='control-label'>Expiration Year</label> <input
                                    class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                    type='text' required>
                            </div>
                        </div>
  
                        <div class='row' style="display:none;">
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert'>Please correct the errors and try
                                    again.</div>
                            </div>
                        </div>

                        <input type="hidden" name="amount" value="{{ $total }}">
  
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now (${{ $total }})</button>
                            </div>
                        </div>
                          
                    </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="https://www.paypal.com/sdk/js?client-id={{env('PAYPAL_SANDBOX_CLIENT_ID')}}"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
<script type="text/javascript">
$(function() {
   
    var $form = $(".require-validation");
   
    $('form.require-validation').bind('submit', function(e) {
        var $form     = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('hide');
  
        $('.has-error').removeClass('has-error');
        $inputs.each(function(i, el) {
          var $input = $(el);
          if ($input.val() === '') {
            $input.parent().addClass('has-error');
            $errorMessage.removeClass('hide');
            e.preventDefault();
          }
        });
   
        if (!$form.data('cc-on-file')) {
          e.preventDefault();
          Stripe.setPublishableKey($form.data('stripe-publishable-key'));
          Stripe.createToken({
            number: $('.card-number').val(),
            cvc: $('.card-cvc').val(),
            exp_month: $('.card-expiry-month').val(),
            exp_year: $('.card-expiry-year').val()
          }, stripeResponseHandler);
        }
  
  });
  
  function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
            /* token contains id, last4, and card type */
            var token = response['id'];
               
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }
   
});
</script>
@endsection