@extends('layouts.master')
@section('title')
Products
@endsection
@section('slider')
<style type="text/css">
    .counter{
     width: 35%;
     display: flex;
     justify-content: space-between;
     align-items: center;
     margin-left: 80px;
     margin-top: -10px;
    }
    
    .count{
     font-size: 17px;
     font-family: ‘Open Sans’;
     font-weight: 900;
     color: #787575;
    }
    
    .counter button {
      border: none;
      border-radius: 5px;
      outline: 0;
      padding: 7px;
      color: white;
      background-color: #6c55f9;
      text-align: center;
      cursor: pointer;
      width: 100%;
      font-size: 13px;
    }
     </style>
    
      <div class="container">
          <div class="page-banner">
            <div class="row justify-content-center align-items-center h-100">
              <div class="col-md-6">
                <nav aria-label="Breadcrumb">
                  <ul class="breadcrumb justify-content-center py-0 bg-transparent">
                    <li class="breadcrumb-item"><a href="{{route('Product')}}">Home</a></li>
                    <li class="breadcrumb-item active">Product</li>
                  </ul>
                </nav>
                <h1 class="text-center">Products</h1>
              </div>
            </div>
          </div>
        </div>
@endsection

@section('content')
<div class="page-section">
    <div class="container">
      @if(Session::has('message'))
       <div class="alert alert-danger">
         {{ Session::get('message')}}
       </div>
      @endif
      @if(Session::has('success'))
       <div class="alert alert-success">
         {{ Session::get('success')}}
       </div>
      @endif
      <div class="row">
          <div class="card-deck">
              @if(count($products) > 0)
              @foreach ($products as $product)
                  <div class="card">
                      <img src="{{ $product->image }}" alt="{{ $product->name }}" height="350">
                      <div class="card-header">
                        {{ $product->name }}
                        <span class="float-right">{{ $product->amount_with_currency }}</span>
                    </div>
                    <div class="card-body">
                        <p>{!!$product->description  !!} </p>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('add-Cart', $product->id) }}" class="btn btn-success btn-block">Add To Cart</a>
                    </div>
                  </div>
              @endforeach
                  
              @endif
          </div>
      </div>
      
    
    </div>
  </div>
@endsection