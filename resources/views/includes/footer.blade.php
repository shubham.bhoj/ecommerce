<footer class="page-footer bg-image" style="background-image: url(../assets/img/world_pattern.svg);">
    <div class="container">
      <div class="row mb-5">
        <div class="col-lg-3 py-3">
          <h3>SEOGram</h3>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero amet, repellendus eius blanditiis in iusto eligendi iure.</p>

          <div class="social-media-button">
            <?php $links = [] ?>
            <a href="" target="_blank"><span class="mai-logo-facebook-f"></span></a>
            <a href="" target="_blank"><span class="mai-logo-twitter"></span></a>
            <a href="" target="_blank"><span class="mai-logo-google-plus-g"></span></a>
            <a href="" target="_blank"><span class="mai-logo-instagram"></span></a>
            <a href="" target="_blank"><span class="mai-logo-youtube"></span></a>
          </div>
        </div>
        <div class="col-lg-3 py-3">
          <h5>Company</h5>
          <ul class="footer-menu">
            <li><a href="#">About Us</a></li>
            <li><a href="#">Career</a></li>
            <li><a href="#">Advertise</a></li>
            <li><a href="#">Terms of Service</a></li>
            <li><a href="#">Help & Support</a></li>
          </ul>
        </div>
        <div class="col-lg-3 py-3">
          <?php 
          $address = [];
          $numbers = [];
          $emails = [];
           ?>
          <h5>Contact Us</h5>
          @foreach($address as $add)
          <p>{{ $add->address}}</p>
          @endforeach
          @foreach($numbers as $num)
          <a href="#" class="footer-link">{{ $num->number}}</a>
          @endforeach
          @foreach($emails as $email)
          <a href="#" class="footer-link">{{ $email->email}}</a>
          @endforeach
        </div>
        <div class="col-lg-3 py-3">
          <h5>Newsletter</h5>
          <p>Get updates, news or events on your mail.</p>
          <form action="" method="post">
            @csrf
            <input type="text" class="form-control" placeholder="Enter your email.." name="emailAddress">
             @if ($errors->has('emailAddress'))
          <span class="text-danger">{{ $errors->first('emailAddress') }}</span>
          @endif
            <button type="submit" class="btn btn-success btn-block mt-2">Subscribe</button>
          </form>

           
          @if(Session::has('success'))
          <div class="alert alert-success">
              {{ Session::get('success')}}
          </div>
         @endif
        </div>
      </div>

      <p class="text-center" id="copyright">Copyright &copy; 2020. This template design and develop by <a href="https://macodeid.com/" target="_blank">MACode ID</a></p>
    </div>
  </footer>