 <!-- Back to top button -->
  <div class="back-to-top"></div>

  <header>
    <nav class="navbar navbar-expand-lg navbar-light bg-white sticky" data-offset="500">
      <div class="container">
        <a href="#" class="navbar-brand">Seo<span class="text-primary">Gram.</span></a>

        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse" id="navbarContent">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item {{ Route::currentRouteName() == 'Product'?'active':''}}">
              <a class="nav-link" href="{{ route('Product')}}">Prodcut</a>
            </li>
            
            <li class="nav-item" style="color:#fff">
                <a class="btn btn-primary ml-lg-2" href="{{ route('Cart') }}"> <i class="fa fa-shopping-cart" style="font-size:22px"></i>&nbsp;
                 @if(session('cart'))
                 {{  count(session('cart')) }}
                 @endif
                </a>
            </li>
          </ul>
        </div>

      </div>
    </nav>

   @yield('slider')
  </header>