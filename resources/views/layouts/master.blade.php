<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <meta name="copyright" content="MACode ID, https://macodeid.com/">

  <title>@yield('title')</title>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="{{ asset('assets/css/maicons.css')}}">

  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css')}}">

  <link rel="stylesheet" href="{{ asset('assets/vendor/animate/animate.css')}}">

  <link rel="stylesheet" href="{{ asset('assets/css/theme.css')}}">

</head>
<body>

  @include('includes/navigation')

  @yield('content')

  @include('includes.footer')

<script src="{{ asset('assets/js/jquery-3.5.1.min.js')}}"></script>

<script src="{{ asset('assets/js/bootstrap.bundle.min.js')}}"></script>

<script src="{{ asset('assets/js/google-maps.js')}}"></script>

<script src="{{ asset('assets/vendor/wow/wow.min.js')}}"></script>

<script src="{{ asset('assets/js/theme.js')}}"></script>
  @yield('script')
</body>
</html>