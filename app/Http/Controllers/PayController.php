<?php

namespace App\Http\Controllers;

use Stripe;
use Illuminate\Http\Request;
use Softon\Indipay\Facades\Indipay;
// use Srmklive\PayPal\Services\ExpressCheckout;
use Srmklive\PayPal\Services\PayPal as PayPalClient;

class PayController extends Controller
{
    public function pay(Request $request)
    {
    //     $params = [
    //         // 'key' => '4AxpyS',
    //         // 'txnid' => '123456789',
    //         // 'hash' => 'defaadgerhetiwerer',
    //         'amount' => $request->amount,
    //         'productinfo' => 'Test Product',
    //         'firstname' => 'Shubham',
    //         'email' => 'product@email.com',
    //         'phone' => '123645789'
    //     ];
    //     $order = Indipay::prepare($params);
    //   return Indipay::process($order);

        // $products = [];

        //     $products['items'] = [
        //         'name' => 'Laravel Book',
        //         'price' => 1200,
        //         'des' => 'Laravel Book for advance learning',
        //         'qty' => '1'
        //     ];
        //     $products['invoice_id'] = uniqid();
        //     $products['invoice_description'] = "Order #{$products['invoice_id']} Billing";
        //     $products['return_url'] = route('pay-success');
        //     $products['cancel_url'] = route('pay-success');
        //     $products['total'] = 1200;
            

            $provider = new PayPalClient;
            $provider->setApiCredentials(config('paypal'));
            $paypalToken = $provider->getAccessToken();
    
            $response = $provider->createOrder([
                "intent" => "CAPTURE",
                "application_context" => [
                    "return_url" => route('successTransaction'),
                    "cancel_url" => route('cancelTransaction'),
                ],
                "purchase_units" => [
                    0 => [
                        "amount" => [
                            "currency_code" => "USD",
                            "value" => $request->amount
                        ]
                    ]
                ],
            ]);


            if (isset($response['id']) && $response['id'] != null) {

                // redirect to approve href
                foreach ($response['links'] as $links) {
                    if ($links['rel'] == 'approve') {
                        return redirect()->away($links['href']);
                    }
                }
    
                return redirect()
                    ->route('Product')
                    ->with('error', 'Something went wrong.');
    
            } else {
                return redirect()
                    ->route('Cart')
                    ->with('error', $response['message'] ?? 'Something went wrong.');
            }

            // $paypal = new ExpressCheckout();
            // $res = $paypal->setExpressCheckout($products);
            // return redirect($res['paypal_link']);
    }

    public function successTransaction(Request $request)
    {

        $provider = new PayPalClient;
        $provider->setApiCredentials(config('paypal'));
        $provider->getAccessToken();
        $response = $provider->capturePaymentOrder($request['token']);

        if (isset($response['status']) && $response['status'] == 'COMPLETED') {
             $cart = session()->get('cart');
             session()->forget('cart');
            return redirect()
                ->route('Product')
                ->with('success', 'Transaction complete.');
        } else {
            return redirect()
                ->route('Cart')
                ->with('error', $response['message'] ?? 'Something went wrong.');
        }
    }

    public function cancelTransaction(Request $request)
    {
        return redirect()
            ->route('Cart')
            ->with('error', $response['message'] ?? 'You have canceled the transaction.');
    }

    public function paymentSuccess(Requset $request)
    {       
            dump($reqest->all());
            $paypal = new ExpressCheckout();
            $response = $paypal->getExpressCheckoutDetalis($request->token);
            dump($response);
    }

    public function response(Request $request)
    {
        $response = Indipay::response($request);
        dd($response);
       
       }

    public function faliure(Request $request)
    {
        dd($request);
    }

    public function payWithStripe(Request $request)
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\PaymentIntent::create([
            'amount' => $request->amount,
            'currency' => 'usd',
            'payment_method_types' => ['card'],
            'description' => 'Testing for Payment'
        ]);
        $cart = session()->get('cart');
        session()->forget('cart');
        return redirect()->route('Product')->with('success', 'Payment SuccessFully.');

    }
}
