<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {   
        return view('product.index')->with('products' , Product::all());
    }

    public function cart()
    {   
        if(count(session()->get('cart')) == null )
        {
          return redirect()->route('Product');
        }else{
          return view('product.cart');
        }
    }

    public function addToCart(Product $product)
    {   
        $cart = session()->get('cart');
        if(!$cart)
        {
            $cart = [
                $product->id => $this->sessionData($product)
                ];
                session()->put('cart',$cart);
                return redirect()->route('Product')->with('success','Added to Cart');
        }

        if(isset($cart[$product->id]))
        {
            $cart[$product->id] ['quantity']++;
            session()->put('cart',$cart);
            return redirect()->route('Product')->with('success','Added to Cart');
        }

        $cart[$product->id] = $this->sessionData($product);
        session()->put('cart',$cart);
        return redirect()->route('Product')->with('success','Added to Cart');
    }

    public function changeQty(Request $request,Product $product)
    {
        $cart = session()->get('cart');
        if($request->change_to === 'down')
        {
            if(isset($cart[$product->id]))
            {
                if($cart[$product->id]['quantity'] > 1)
                {
                    $cart[$product->id]['quantity']--;
                    return $this->setSessionAndReturnResponse($cart);
                }else{
                    return $this->removeFromCart($product->id);
                }
            }
        }else{

            if(isset($cart[$product->id]))
            {
                $cart[$product->id]['quantity']++;
                return $this->setSessionAndReturnResponse($cart);
            }
        }
    }

    public function removeFromCart($id)
    {
        $cart = session()->get('cart');
        if(isset($cart[$id])){
            unset($cart[$id]);
            session()->put('cart',$cart);
        }

        return redirect()->route('Cart')->with('success','Remove from Cart');
    }

    protected function sessionData(Product $product)
    {
        return  [
            'id' => $product->id,
            'name' => $product->name,
            'quantity' => 1,
            'price' => $product->price,
            'image' => $product->image
        ];
    }

    protected function setSessionAndReturnResponse($cart)
    {
        session()->put('cart',$cart);
        return redirect()->route('Cart');

    }

}
