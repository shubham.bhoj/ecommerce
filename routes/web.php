<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PayController;
use App\Http\Controllers\ProductController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [ProductController::class, 'index'])->name('Product');
Route::get('/cart', [ProductController::class, 'cart'])->name('Cart');
Route::get('/add-to-cart/{product}', [ProductController::class, 'addToCart'])->name('add-Cart');
Route::get('/remove/{id}', [ProductController::class, 'removeFromCart'])->name('remove');

Route::get('/change-qty/{product}',[ProductController::class, 'changeQty'])->name('change_qty');

Route::post('/pay',[PayController::class,'pay'])->name('Pay');
Route::post('/indipay/response/success', [PayController::class,'response'])->name('Pay.Response');
Route::post('/indipay/response/failure', [PayController::class,'faliure'])->name('Pay.Faliure');

Route::get('success-transaction', [PayController::class, 'successTransaction'])->name('successTransaction');
Route::get('cancel-transaction', [PayController::class, 'cancelTransaction'])->name('cancelTransaction');

Route::get('payment-success',[PayController::class,'paymentSuccess'])->name('pay-success');

route::post('pay-stripe',[PayController::class,'payWithStripe'])->name('Pay_Stripe');

